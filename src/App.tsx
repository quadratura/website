import React, { useMemo } from 'react';
import styled from '@emotion/styled';
import { Global } from '@emotion/core';
import { ApolloProvider } from '@apollo/react-hooks';
import { AUTH0_DOMAIN, AUTH0_CLIENT_ID, AUTH0_AUDIENCE } from './config';
import { Router, Switch, Redirect } from 'react-router-dom';
import client from './services/api';
import Home from './pages/home';
import { dark, light } from './theme';
import { useThemeToggler } from './hooks/theme_toggler';
import ThemeContext from './context/theme_context';
import './App.css';
import { Auth0Provider } from './security/authWrapper';
import history, { onRedirectCallback } from './security/history';
import SecureRoute from './security/SecureRoute';

const Page = styled.div`
  display: grid;
  height: 100vh;
`;

const App: React.FC = () => {
  const [darkModeOn, toggleDarkMode] = useThemeToggler();
  const value = useMemo(
    () => ({
      darkModeOn,
      toggleDarkMode,
    }),
    [darkModeOn, toggleDarkMode]
  );
  return (
    <Auth0Provider
      domain={AUTH0_DOMAIN}
      client_id={AUTH0_CLIENT_ID}
      audience={AUTH0_AUDIENCE}
      redirect_uri={window.location.origin}
      onRedirectCallback={onRedirectCallback}
    >
      <ApolloProvider client={client}>
        <ThemeContext.Provider value={value}>
          <Router history={history}>
            <Global styles={darkModeOn ? dark : light} />
            <Page>
              <Switch>
                <SecureRoute
                  exact
                  path="/"
                  component={() => <Redirect to="/explore" />}
                />
                <SecureRoute path="/explore" component={Home} />
              </Switch>
            </Page>
          </Router>
        </ThemeContext.Provider>
      </ApolloProvider>
    </Auth0Provider>
  );
};

export default App;
