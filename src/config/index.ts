export const IN_DARK_MODE = Boolean(process.env.REACT_APP_IN_DARK_MODE);
export const API_URL = process.env.REACT_APP_API_URL || '';
export const API_KEY = process.env.REACT_APP_API_KEY || '';
export const BELGRADE_COORDINATES = {
  lat: 44.8120852,
  lng: 20.448123,
};
export const SENTRY_DSN = process.env.REACT_APP_SENTRY_DSN || '';
export const AUTH0_DOMAIN = process.env.REACT_APP_AUTH0_DOMAIN || '';
export const AUTH0_CLIENT_ID = process.env.REACT_APP_AUTH0_CLIENT_ID || '';
export const AUTH0_AUDIENCE = process.env.REACT_APP_AUTH0_AUDIENCE || '';
