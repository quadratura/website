import { createContext } from "react";
import { IN_DARK_MODE } from "../../config";

const ThemeContext = createContext({
  darkModeOn: IN_DARK_MODE,
  toggleDarkMode: () => {},
});

export default ThemeContext;
