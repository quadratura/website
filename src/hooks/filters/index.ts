import { useReducer } from "react";
import {
  RENT_TAG,
  SELL_TAG,
  AGENCY_TAG,
  OWNER_TAG,
  FURNISHED_TAG,
  HALF_FURNISHED_TAG,
  NOT_FURNISHED_TAG,
  LUX_CONDITION_TAG,
  RENOVATED_CONDITION_TAG,
} from "./tags";
export * from "./tags";

export interface ITag {
  kind: string;
  text: string;
}
export interface IState {
  tags: ITag[];
  price_range: {
    enabled: boolean;
    start: number;
    end: number;
  };
}
export interface IAction {
  type: string;
  payload: any;
}

const MUTUALLY_EXCLUSIVE = [
  [HALF_FURNISHED_TAG, NOT_FURNISHED_TAG, FURNISHED_TAG],
  [AGENCY_TAG, OWNER_TAG],
  [RENT_TAG, SELL_TAG],
  [LUX_CONDITION_TAG, RENOVATED_CONDITION_TAG],
];

export function useFilters(state: IState) {
  const initState = state
    ? state
    : {
        tags: [],
        price_range: {
          enabled: false,
          start: 0,
          end: 0,
        },
      };
  return useReducer((state: IState, action: IAction): IState => {
    const { type } = action;
    if (type === "add_tag") {
      return {
        ...state,
        tags: [
          ...filter_out(state.tags, find_tags_to_exclude(action.payload)),
          action.payload,
        ],
      };
    }

    if (type === "remove_tag") {
      return {
        ...state,
        tags: state.tags.filter(tag => {
          return tag !== action.payload;
        }),
      };
    }

    if (type === "toggle_price_range") {
      return {
        ...state,
        price_range: {
          ...state.price_range,
          enabled: !state.price_range.enabled,
        },
      };
    }

    if (type === "set_price_range_start") {
      return {
        ...state,
        price_range: {
          ...state.price_range,
          start: action.payload,
        },
      };
    }

    if (type === "set_price_range_end") {
      return {
        ...state,
        price_range: {
          ...state.price_range,
          end: action.payload,
        },
      };
    }

    throw new Error("Invalid action provided");
  }, initState);
}

function filter_out(ar: ITag[], tags: ITag[]) {
  return ar.filter(tag => !Boolean(tags.includes(tag)));
}

function find_tags_to_exclude(tag: ITag) {
  return MUTUALLY_EXCLUSIVE.filter((tags: ITag[]) => {
    return tags.includes(tag);
  })
    .flat()
    .filter(t => Boolean(t !== tag));
}
