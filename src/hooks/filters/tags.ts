export const RENT_TAG = { kind: 'purchase_option', text: 'rent' };
export const SELL_TAG = { kind: 'purchase_option', text: 'sell' };

export const AGENCY_TAG = { kind: 'landlord', text: 'agency' };
export const OWNER_TAG = { kind: 'landlord', text: 'owner' };

export const FURNISHED_TAG = {
  kind: 'furnished_status',
  text: 'fully_furnished',
};
export const HALF_FURNISHED_TAG = {
  kind: 'furnished_status',
  text: 'semi_furnished',
};
export const NOT_FURNISHED_TAG = {
  kind: 'furnished_status',
  text: 'not_furnished',
};

export const LUX_CONDITION_TAG = { kind: 'property_condition', text: 'lux' };
export const RENOVATED_CONDITION_TAG = {
  kind: 'property_condition',
  text: 'renovated',
};

export const GARAGE_TAG = { kind: 'generic', text: 'Garaža' };
export const ELEVATOR_TAG = { kind: 'generic', text: 'Lift' };
export const PENTHOUSE_TAG = { kind: 'generic', text: 'Penthouse' };
