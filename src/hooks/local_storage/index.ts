import React from 'react';
import storage from '../../services/storage';

const noop: Function = () => {};
export default function useLocalStorage<T = any>(
  key: string,
  optionalCallback = noop
): [T | any, (args: T | any) => void, () => void] {
  const [state, setState] = React.useState(null);
  React.useEffect(() => {
    const load = async () => {
      const parsedValue = await storage.get<any>(key, null);
      if (parsedValue !== null) {
        setState(parsedValue);
        optionalCallback(parsedValue);
      }
    };
    load();
  }, []);
  const removeItem = () => {
    setState(null);
    storage.remove(key).then(() => optionalCallback(null));
  };
  const setItem = (obj: any) => {
    setState(obj);
    storage.set(key, obj).then(() => optionalCallback(obj));
  };
  return [state, setItem, removeItem];
}
