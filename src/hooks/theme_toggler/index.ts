import { useState, useEffect } from 'react';
import storage from '../../services/storage';
import { IN_DARK_MODE } from '../../config';

export function useThemeToggler(): [boolean, () => Promise<void>] {
  const [darkModeOn, setDarkModeOn] = useState<boolean>(IN_DARK_MODE);
  useEffect(() => {
    const loadFromCache = async () => {
      const darkMode = await storage.get<boolean>('dark-mode', IN_DARK_MODE);
      setDarkModeOn(darkMode);
    };

    loadFromCache();
  }, [darkModeOn]);

  return [
    darkModeOn,
    async () => {
      setDarkModeOn(!darkModeOn);
      await storage.set('dark-mode', !darkModeOn);
      window.location.reload();
    },
  ];
}
