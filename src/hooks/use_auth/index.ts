import { useState, useEffect } from 'react';
import storage from '../../services/storage';

export interface IAuthUser {
  name: string;
  email: string;
}
export interface IToken {
  accessToken: {
    accessToken: string;
  };
  idToken: {
    claims: any;
  };
}
export default function useAuth(): [string?, IAuthUser?] {
  const [accessToken, setAccessToken] = useState<string | undefined>(undefined);
  const [user, setUser] = useState<IAuthUser | undefined>(undefined);

  useEffect(() => {
    const loadFromCache = async () => {
      const { accessToken, idToken } = await storage.get<IToken>(
        'okta-token-storage',
        {
          accessToken: {
            accessToken: '',
          },
          idToken: {
            claims: {},
          },
        }
      );

      setAccessToken(accessToken.accessToken);
      setUser(idToken.claims);
    };

    loadFromCache();
  }, []);

  return [accessToken, user];
}
