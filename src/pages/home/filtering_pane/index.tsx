import React from 'react';
import { Button, Input, InputNumber, Switch } from 'antd';
import styled from '@emotion/styled';
import {
  useFilters,
  ITag,
  RENT_TAG,
  SELL_TAG,
  AGENCY_TAG,
  OWNER_TAG,
  FURNISHED_TAG,
  HALF_FURNISHED_TAG,
  NOT_FURNISHED_TAG,
  LUX_CONDITION_TAG,
  RENOVATED_CONDITION_TAG,
  GARAGE_TAG,
  ELEVATOR_TAG,
  PENTHOUSE_TAG,
  IState,
} from '../../../hooks/filters';

const ButtonGroup = Button.Group;
const InputGroup = Input.Group;

const FilterContainer = styled.div`
  display: grid;
  grid-template-columns: auto auto auto;
  grid-template-rows: auto auto auto;
  grid-gap: 30px;
`;

interface IProps {
  isActive: (tag: ITag) => boolean;
  onActivate: (tag: ITag) => any;
  onDeactivate: (tag: ITag) => any;
  tag: ITag;
}
const Toggle: React.FC<IProps> = ({
  isActive,
  onActivate,
  onDeactivate,
  tag,
}) => {
  const active = isActive(tag);
  const onClick = () => (active ? onDeactivate(tag) : onActivate(tag));
  return (
    <Button size="large" type={active ? 'primary' : 'dashed'} onClick={onClick}>
      {tag.text.charAt(0).toLocaleUpperCase() +
        tag.text.replace(/_/g, ' ').slice(1)}
    </Button>
  );
};

export interface IFormState {
  tags?: ITag[];
  priceRange?: { start: number; end: number };
}
const FilteringPane: React.FC<{
  state: IState;
  onSubmit: (state: IFormState) => void;
  onCancel: () => void;
}> = ({ onSubmit, onCancel, state: initState }) => {
  const [state, dispatch] = useFilters(initState);

  const add = (tag: ITag) => dispatch({ type: 'add_tag', payload: tag });
  const remove = (tag: ITag) => dispatch({ type: 'remove_tag', payload: tag });
  const isSelected = (tag: ITag) => state.tags.includes(tag);
  const renderToggleButton = (tag: ITag) => (
    <Toggle
      tag={tag}
      isActive={isSelected}
      onActivate={add}
      onDeactivate={remove}
    />
  );

  return (
    <>
      <FilterContainer>
        <div>
          <h2>Purchase Option</h2>
          <ButtonGroup>
            {renderToggleButton(RENT_TAG)}
            {renderToggleButton(SELL_TAG)}
          </ButtonGroup>
        </div>
        <div>
          <h2>Keeper</h2>
          <ButtonGroup>
            {renderToggleButton(AGENCY_TAG)}
            {renderToggleButton(OWNER_TAG)}
          </ButtonGroup>
        </div>
        <div>
          <h2>Furnishings</h2>
          <ButtonGroup>
            {renderToggleButton(NOT_FURNISHED_TAG)}
            {renderToggleButton(HALF_FURNISHED_TAG)}
            {renderToggleButton(FURNISHED_TAG)}
          </ButtonGroup>
        </div>
        <div>
          <h2>Property Condition</h2>
          <ButtonGroup>
            {renderToggleButton(LUX_CONDITION_TAG)}
            {renderToggleButton(RENOVATED_CONDITION_TAG)}
          </ButtonGroup>
        </div>
        <div>
          <h2>Extras</h2>
          <ButtonGroup>
            {renderToggleButton(GARAGE_TAG)}
            {renderToggleButton(ELEVATOR_TAG)}
            {renderToggleButton(PENTHOUSE_TAG)}
          </ButtonGroup>
        </div>
        <div>
          <h2>
            Price Range
            <span>
              <Switch
                checked={state.price_range.enabled}
                size="small"
                onChange={() =>
                  dispatch({ type: 'toggle_price_range', payload: null })
                }
              />
            </span>
          </h2>

          {state.price_range.enabled && (
            <InputGroup compact>
              <InputNumber
                size="large"
                step={isSelected(RENT_TAG) ? 100 : 10000}
                min={0}
                defaultValue={state.price_range.start}
                onChange={value =>
                  dispatch({
                    type: 'set_price_range_start',
                    payload: value ? value : 0,
                  })
                }
              />
              <InputNumber
                size="large"
                step={isSelected(RENT_TAG) ? 100 : 10000}
                min={100}
                defaultValue={state.price_range.end}
                onChange={value =>
                  dispatch({
                    type: 'set_price_range_end',
                    payload: value ? value : 0,
                  })
                }
              />
            </InputGroup>
          )}
        </div>
        <ButtonGroup>
          <Button
            shape="round"
            size="large"
            type="primary"
            onClick={() => onSubmit(state)}
          >
            Filter
          </Button>
          <Button shape="round" size="large" onClick={onCancel}>
            Close
          </Button>
        </ButtonGroup>
      </FilterContainer>
    </>
  );
};

export default FilteringPane;
