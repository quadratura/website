import React, { useState } from 'react';
import styled from '@emotion/styled';
import GoogleMapReact from 'google-map-react';
import { useQuery } from '@apollo/react-hooks';
import SuperCluster from 'supercluster';
import { Button, Popover, List, Drawer, message } from 'antd';
import {
  FilterOutlined,
  FunnelPlotOutlined,
  SearchOutlined,
  SettingOutlined,
  CloseOutlined,
} from '@ant-design/icons';
import { useThemeToggler } from '../../hooks/theme_toggler';
import { API_KEY, BELGRADE_COORDINATES } from '../../config';
import { dark, light } from './google_maps_color_schemes';
import money_format from '../../services/money_formatter';
import { ALL_FLATS_QUERY, IApiResults, IApiFlat } from './query';
import FilteringPane from './filtering_pane';
import SearchBox from './search_box';
import { useAuth0 } from '../../security/authWrapper';

const PageContainer = styled.div`
  width: 100%;
`;
const GoogleMapContainer = styled.div`
  width: 100%;
  height: 100%;
`;
const SearchFilterControls = styled.div`
  display: grid;
  grid-gap: 10px;
  position: fixed;
  top: 50px;
  left: 50px;
`;
const PreferencesControls = styled.div`
  position: fixed;
  right: 50px;
  top: 50px;
  gap: 10px;
  display: grid;
  & > * {
    display: flex;
    justify-content: flex-end;
  }
`;

const Marker: any = ({ children, lat, lng, key }: any) => {
  return (
    <div key={key} {...{ lat, lng }}>
      {children}
    </div>
  );
};

const MarkerGroup = (props: any) => {
  const content = (
    <List
      rowKey="url"
      pagination={{
        position: 'bottom',
      }}
      dataSource={props.markers.sort(
        (a: any, b: any) => Number(a.price) - Number(b.price)
      )}
      renderItem={(marker: any) => (
        <List.Item key={marker.url}>
          <a href={marker.url} rel="noopener noreferrer" target="_blank">
            {money_format.format(Number(marker.price))}
          </a>
        </List.Item>
      )}
    />
  );

  return (
    <Marker key={props.key}>
      <Popover content={content} trigger="click">
        <Button type="danger" shape="circle">
          {props.count}
        </Button>
      </Popover>
    </Marker>
  );
};

interface IProps {
  accessToken: string;
  logoutCallback: () => void;
}
const Home: React.FC<IProps> = ({ accessToken, logoutCallback }) => {
  const [darkModeOn, toggleDarkMode] = useThemeToggler();
  const [coordinates, setCoordinates] = useState();
  const [preferencesVisible, setPreferencesVisible] = useState(false);
  const [filtersShown, setFiltersShown] = useState(false);
  const [mapCenter, setMapCenter] = useState();
  const [mapZoom, setMapZoom] = useState();
  const [filters, setFilters] = useState({
    tags: [],
    price_range: { enabled: false, start: 0, end: 100 },
  });

  const [searchBounds, setSearchBounds] = useState();
  const { loading, error, data } = useQuery<IApiResults>(ALL_FLATS_QUERY, {
    variables: {
      geo_location: searchBounds,
      tags: filters.tags.length ? filters.tags : undefined,
      price_range:
        searchBounds && filters.price_range.enabled
          ? {
              start: filters.price_range.start,
              end: filters.price_range.end,
            }
          : undefined,
    },
    context: {
      headers: { Authorization: `Bearer ${accessToken}` },
    },
    onCompleted: data => {
      if (!searchBounds) {
        return;
      }
      const noRecords = new Intl.NumberFormat().format(data.search.length);
      message.success(`${noRecords} search results found.`);
    },
  });

  if (error) {
    console.error(error);
    message.error(error.message);
  }

  const resolveBounds = (coordinates: any) => {
    if (coordinates === undefined) {
      return undefined;
    }

    if (coordinates.bounds === undefined) {
      return undefined;
    }

    return coordinates.bounds;
  };
  const resolveZoom = (coordinates: any) => {
    if (coordinates === undefined) {
      return undefined;
    }
    if (coordinates.zoom === undefined) {
      return undefined;
    }

    return coordinates.zoom;
  };
  const bounds = resolveBounds(coordinates);
  const zoom = resolveZoom(coordinates);
  const applyFilters = function(filters: any) {
    setFilters(filters);
    setSearchBounds(bounds);
    setFiltersShown(false);
  };
  const applyNewSearchBounds = function() {
    setSearchBounds(bounds);
  };

  const list_of_flats = data && data.search ? data.search : [];
  const index = new SuperCluster<IApiFlat>({
    radius: 130,
    maxZoom: 20,
  });
  const flatsWithPoint: any = list_of_flats.map(item => {
    return {
      ...item,
      geometry: {
        coordinates: item.geo_location.coordinates,
      },
    };
  });
  index.load(flatsWithPoint);

  return (
    <PageContainer>
      <Drawer
        placement={'top'}
        closable={true}
        height={'auto'}
        onClose={() => setFiltersShown(false)}
        visible={filtersShown}
        destroyOnClose={true}
      >
        <FilteringPane
          state={filters}
          onCancel={() => setFiltersShown(false)}
          onSubmit={applyFilters}
        />
      </Drawer>
      <GoogleMapContainer>
        <GoogleMapReact
          bootstrapURLKeys={{ key: API_KEY }}
          defaultCenter={BELGRADE_COORDINATES}
          center={mapCenter}
          zoom={mapZoom}
          defaultZoom={16}
          onChange={(data: any): void => setCoordinates(data)}
          options={{
            styles: darkModeOn ? dark : light,
            gestureHandling: 'greedy',
            mapTypeControl: true,
            fullscreenControl: false,
            mapTypeControlOptions: {
              position: 6,
              style: 3,
            },
            // streetView: true,
            streetViewControl: true,
            disableDoubleClickZoom: true,
          }}
        >
          {bounds &&
            zoom &&
            index
              .getClusters(
                [bounds.nw.lng, bounds.sw.lat, bounds.se.lng, bounds.ne.lat],
                zoom
              )
              .map((cluster: any) => {
                if (cluster.type === 'Feature') {
                  return (
                    <MarkerGroup
                      key={cluster.id}
                      markers={index.getLeaves(cluster.id, Infinity)}
                      lat={cluster.geometry.coordinates[1]}
                      lng={cluster.geometry.coordinates[0]}
                      count={cluster.properties.point_count}
                    />
                  );
                } else if (cluster.price !== undefined) {
                  return (
                    <Marker
                      key={cluster.url}
                      lat={cluster.geo_location.coordinates[1]}
                      lng={cluster.geo_location.coordinates[0]}
                    >
                      <Button
                        type={'danger'}
                        size="small"
                        href={cluster.url}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <strong>
                          {money_format.format(Number(cluster.price))}
                        </strong>
                      </Button>
                    </Marker>
                  );
                }
                return null;
              })}
        </GoogleMapReact>
      </GoogleMapContainer>
      <SearchFilterControls>
        <div>
          <Button
            // disabled={loading}
            type="primary"
            icon={
              filters.tags.length ? <FunnelPlotOutlined /> : <FilterOutlined />
            }
            size="large"
            shape="round"
            onClick={() => setFiltersShown(true)}
          >
            {filters.tags.length ? `Filter (${filters.tags.length})` : 'Filter'}
          </Button>
        </div>
        <div>
          <Button
            loading={loading}
            // disabled={loading}
            shape="round"
            type="primary"
            size="large"
            icon={<SearchOutlined />}
            onClick={applyNewSearchBounds}
          >
            {loading ? 'Searching' : 'Search This Area'}
          </Button>
        </div>
        <div>
          <SearchBox
            onSelect={({ center, zoom }: any) => {
              setMapCenter(center);
              setMapZoom(zoom);
            }}
          />
        </div>
      </SearchFilterControls>
      <PreferencesControls>
        <div>
          <Button
            type="primary"
            size="large"
            shape="circle"
            onClick={() => setPreferencesVisible(!preferencesVisible)}
            icon={preferencesVisible ? <CloseOutlined /> : <SettingOutlined />}
          ></Button>
        </div>
        {preferencesVisible && (
          <>
            <div>
              <Button onClick={toggleDarkMode}>
                {darkModeOn
                  ? 'Switch To Light Mode'
                  : 'Switch To Dark Mode (beta)'}
              </Button>
            </div>
            <div>
              <Button onClick={logoutCallback}>Logout</Button>
            </div>
          </>
        )}
      </PreferencesControls>
    </PageContainer>
  );
};

const WrapperComponent: React.FC = () => {
  const { isAuthenticated, getTokenSilently, logout } = useAuth0();
  const [accessToken, setAccessToken] = useState('');
  React.useEffect(() => {
    if (isAuthenticated) {
      getTokenSilently().then(setAccessToken);
    }
  }, [isAuthenticated, getTokenSilently]);

  if (Boolean(accessToken)) {
    return <Home accessToken={accessToken} logoutCallback={logout} />;
  }

  return <span>Loading...</span>;
};

export default WrapperComponent;
