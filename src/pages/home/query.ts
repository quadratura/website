import { gql } from 'apollo-boost';

export const ALL_FLATS_QUERY = gql`
  query ALL_FLATS(
    $geo_location: GeoLocationQuery
    $tags: [TagQuery]
    $price_range: PriceRange
    $skip: Int
    $limit: Int
  ) {
    search(
      geo_location: $geo_location
      tags: $tags
      price_range: $price_range
      limit: $limit
      skip: $skip
    ) {
      price
      url
      geo_location {
        coordinates
      }
    }
  }
`;

export interface IApiFlat {
  price: string;
  url: string;
  geo_location: {
    coordinates: [number, number];
  };
}
export interface IApiResults {
  search: IApiFlat[];
}
