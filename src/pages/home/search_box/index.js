import React from 'react';
import { Input, message, List, Typography } from 'antd';
import { EnvironmentOutlined } from '@ant-design/icons';
import { fitBounds } from 'google-map-react/utils';
import PlacesAutocomplete, {
  geocodeByAddress,
} from 'react-places-autocomplete';

const { Text } = Typography;
class LocationSearchInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { address: '' };
  }

  handleChange = address => {
    this.setState({ address });
  };

  handleSelect = async address => {
    try {
      this.setState({ address: '' });

      const width = window.innerWidth;
      const height = window.innerHeight;
      const results = await geocodeByAddress(address);
      const { bounds } = results[0].geometry;
      const { center, zoom } = fitBounds(
        {
          ne: {
            lat: bounds.getNorthEast().lat(),
            lng: bounds.getNorthEast().lng(),
          },
          sw: {
            lat: bounds.getSouthWest().lat(),
            lng: bounds.getSouthWest().lng(),
          },
        },
        {
          width,
          height,
        }
      );
      this.props.onSelect({ center, zoom });
    } catch (e) {
      console.error(e);
      message.error(e.message);
    }
  };

  componentDidCatch(error, errorInfo) {
    console.error(error, errorInfo);
  }

  render() {
    return (
      <PlacesAutocomplete
        value={this.state.address}
        onChange={this.handleChange}
        onSelect={this.handleSelect}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
          <div>
            <Input
              prefix={<EnvironmentOutlined />}
              size="large"
              placeholder="Find City"
              {...getInputProps()}
            />
            {Boolean(suggestions.length) && (
              <List bordered={true} loading={loading} size="large">
                {suggestions.map(suggestion => {
                  const className = suggestion.active
                    ? 'suggestion-item--active'
                    : 'suggestion-item';
                  return (
                    <List.Item
                      {...getSuggestionItemProps(suggestion, { className })}
                    >
                      <div>
                        <Text strong>
                          {suggestion.formattedSuggestion.mainText}
                        </Text>
                        <br />
                        <Text type="secondary">
                          {suggestion.formattedSuggestion.secondaryText}
                        </Text>
                      </div>
                    </List.Item>
                  );
                })}
              </List>
            )}
          </div>
        )}
      </PlacesAutocomplete>
    );
  }
}

export default LocationSearchInput;
