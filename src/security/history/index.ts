import { createBrowserHistory } from 'history';

const history = createBrowserHistory();
export default history;

// A function that routes the user to the right place
// after login
export const onRedirectCallback = (appState: any): void => {
  history.push(
    appState && appState.targetUrl
      ? appState.targetUrl
      : window.location.pathname
  );
};
