const formatter = new Intl.NumberFormat("sr-RS", {
  style: "currency",
  currency: "EUR",
  minimumFractionDigits: 0
});

export default formatter;
