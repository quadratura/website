export interface Storage {
  set<T>(key: string, item: T): Promise<T>;
  get<T, K = T>(key: string, default_value: K): Promise<T | K | undefined>;
}

class LocalStorage implements Storage {
  async set<T>(key: string, value: T): Promise<T> {
    window.localStorage.setItem(key, JSON.stringify(value));

    return value;
  }

  async get<T, K = T>(key: string, default_value: K): Promise<T | K> {
    if (!window.localStorage.hasOwnProperty(key)) {
      return default_value;
    }
    const value = window.localStorage.getItem(key);

    if (value === null) {
      return default_value;
    }

    return JSON.parse(value);
  }

  async remove(key: string): Promise<boolean> {
    if (window.localStorage.hasOwnProperty(key)) {
      window.localStorage.removeItem(key);
    }

    return true;
  }
}

export default new LocalStorage();
