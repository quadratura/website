import { css } from '@emotion/core';

export const light = css`
  @import url('https://cdnjs.cloudflare.com/ajax/libs/antd/4.0.0/antd.min.css');
  * {
    --app-header-height: 45px;
    --app-color-1: white;
    --app-color-2: #cecece;
  }
`;
export const dark = css`
  @import url('https://cdnjs.cloudflare.com/ajax/libs/antd/4.0.0/antd.dark.min.css');
  * {
    --app-header-height: 45px;
    --app-color-1: #333;
    --app-color-2: #cecece;
  }
`;
